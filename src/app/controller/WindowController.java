package app.controller;

import app.view.Window;
import com.l2fprod.common.swing.JTaskPane;
import com.l2fprod.common.swing.JTaskPaneGroup;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

public class WindowController implements ActionListener {

    private Window window;

    public WindowController(Window window) {
        this.window = window;
    }

    private void save() throws FileNotFoundException {
        PrintWriter file = new PrintWriter("table.txt");
        if (JOptionPane.showConfirmDialog(null, "Are you sure?", "Warning",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            for (int row = 0; row < window.getTable().getRowCount(); row++) {
                for (int col = 0; col < window.getTable().getColumnCount(); col++) {
                    file.println(window.getTable().getValueAt(row, col));
                }
            }
            JOptionPane.showMessageDialog(null, "Save succeed!");
        }
        file.close();
    }


    private void writingToTable() {
        double liczba = 0.0;
        try {
            liczba = Double.parseDouble(window.getInputNumber().getText());
        } catch (NumberFormatException e) {
            System.out.println("Eksepszyn" + e);
            JOptionPane.showMessageDialog(null,
                    "Unknown: " + e + ". Please use only numbers!",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        }

        window.getTable().setValueAt(liczba, window.getRow() - 1, window.getCol() - 1);
    }

    private void resettingTable() {
        for (int row = 0; row < window.getTable().getRowCount(); row++) {
            for (int col = 0; col < window.getTable().getColumnCount(); col++) {
                window.getTable().setValueAt(0.0, row, col);
            }
        }
    }


    private void creatingActionsDependOnChoice() {
        if (window.getList().getSelectedIndex() == 0) {     //sum
            expectedSum();
        } else if (window.getList().getSelectedIndex() == 1) {  //average
            expectedAv();
        } else if (window.getList().getSelectedIndex() == 2) {      //mmin max
            expectedMINMAX();
        }
    }

    private void expectedSum() {
        double total = 0;
        for (int i = 0; i < window.getTable().getRowCount(); i++) {
            for (int col = 0; col < window.getTable().getColumnCount(); col++) {
                // null or not Integer will throw exception
                total += (Double) window.getTable().getValueAt(col, i);
            }
        }
        window.getTextArea().setText(Double.toString(total));
    }

    private void expectedAv() {
        double total = 0;
        for (int i = 0; i < window.getTable().getRowCount(); i++) {
            for (int col = 0; col < window.getTable().getColumnCount(); col++) {
                // null or not Integer will throw exception
                total += (Double) window.getTable().getValueAt(col, i);
            }
        }
        total = total / (window.getTable().getRowCount() * window.getTable().getColumnCount());
        window.getTextArea().setText(Double.toString(total));
    }

    private void expectedMINMAX() {
        double max = 0.0;
        for (int i = 0; i < window.getTable().getRowCount(); i++) {
            for (int col = 0; col < window.getTable().getColumnCount(); col++) {
                double currentValue = (Double) window.getTable().getValueAt(col, i);
                if (currentValue > max) {
                    max = currentValue;
                }
            }
        }

        double min = (double) window.getTable().getValueAt(0, 0);
        for (int i = 0; i < window.getTable().getRowCount(); i++) {
            for (int col = 0; col < window.getTable().getColumnCount(); col++) {
                if ((Double) window.getTable().getValueAt(i, col) < min) {
                    min = (Double) window.getTable().getValueAt(i, col);

                } else {
                    col++;
                }
            }
        }

        window.getTextArea().setText("MIN: " + min + "\nMAX: " + max);
    }

    private void setRandomTable() {
        Random random = new Random();
        for (int i = 0; i < window.getTable().getRowCount(); i++) {
            for (int col = 0; col < window.getTable().getColumnCount(); col++) {
                window.getTable().setValueAt(Math.abs(random.nextDouble() % 100.0 * 10), i, col);
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if ("SAVE".equals(e.getActionCommand())) {
            try {
                save();
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
        } else if ("OK".equals(e.getActionCommand())) {
            writingToTable();
        } else if ("RESET".equals(e.getActionCommand())) {
            resettingTable();
        } else if ("SUM".equals(e.getActionCommand())) {
            window.getTextArea().setText(null);
            expectedSum();
        } else if ("AV".equals(e.getActionCommand())) {
            window.getTextArea().setText(null);
            expectedAv();
        } else if ("MINMAX".equals(e.getActionCommand())) {
            window.getTextArea().setText(null);
            expectedMINMAX();
        } else if ("RESULT".equals(e.getActionCommand())) {
            window.getTextArea().setText(null);
            creatingActionsDependOnChoice();
        } else if ("ABOUT".equals((e.getActionCommand()))) {
            creatingAboutPage();
        } else if ("INSTRUCTION".equals((e.getActionCommand()))) {
            creatingInstructionPage();
        } else if ("RANDOM".equals((e.getActionCommand()))) {
            setRandomTable();
        } else if ("EXIT".equals(e.getActionCommand())) {
            if (JOptionPane.showConfirmDialog(null, "Are you sure?", "Warning",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                System.exit(0);
            }
        }
    }

    private void creatingAboutPage() {

        try {
            Image img = ImageIO.read(getClass().getResource("../../foto.jpg"));
            ImageIcon icon = new ImageIcon();
            icon.setImage(img);
            JOptionPane.showMessageDialog(null,
                    "Author: Aleksandra Skoczypiec \nversion 1.0 \nEmail: o.skoczypiec@gmail.com",
                    "ABOUT",
                    JOptionPane.INFORMATION_MESSAGE,
                    icon);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void creatingInstructionPage() {
        JOptionPane.showMessageDialog(null,
                "How to use this app?\nFirstly, you have to choose where you want to insert your value\n" +
                        "To set this use slidebars.\nSecondly, in text field enter your value and confirm it with 'ok' button\n" +
                        "Repeat previous steps as much as you want.\nNow you can choose operations: sum, average, random, reset or min,max.\n" +
                        "For any problems, please e-mail me.\n",
                "INSTRUCTION",
                JOptionPane.INFORMATION_MESSAGE);
    }


}

/*
public abstract class AbstractClass {
    public void saySomething() {
        System.out.println("Something");
    }

    public abstract void saySomethingUsefull();
}*/


/*
public class Main {

  */
/* NWD - program wyznacający największy wspólny dzielnik dwóch liczb
   * podanych jako parametry funkcji - algorytm Euklidesa
   *//*


    */
/*
     * pierwsza wersja funkcji, w której zastosowano wielokrotne odejmowanie
     * dwóch liczb
     *//*

    public static int NWD_1(int pierwsza, int druga)
    {
        while (pierwsza != druga) // dopóki dwie liczby nie są sobie równe
        {
            if (pierwsza > druga)  // sprawdzamy, która z nich jest większa
            {
                pierwsza = pierwsza - druga; // odejmujemy mniejszą liczbę
            }                               // od większej
            else
            {
                druga = druga - pierwsza;
            }
        }
        return pierwsza;
    }

    */
/*
     * druga wersja algorytmu Euklidesa, w której zastosowano rekurencję
     * oraz operację modulo dwóch lich
     *//*

    public static int NWD_2(int pierwsza, int druga)
    {
        if (druga == 0)
        {
            return pierwsza;
        }
        else // rekurencyjne wywołanie funkcji, gdzie kolejność parametrów
        {   // została zamieniona, dodatkowo drugi parametr to operacja modulo
            return NWD_2(druga, pierwsza%druga);  // dwóch liczb.
        }
    }


    public static void main(String[] args)
    {
        System.out.println(NWD_1(130,20)); // przykład zastosowania podejścia 1
        System.out.println(NWD_2(130,20));  // oraz 2
    }
}*/
