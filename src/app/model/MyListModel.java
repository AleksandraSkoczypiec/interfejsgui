package app.model;

import javax.swing.*;

public class MyListModel<T> extends DefaultListModel<T>{

    /**
     * Wywołanie konstruktora z klasy nadrzędnej
     */
    public MyListModel() {
        super();
    }

    public void addElements(T... t){
        for (T c : t) {
            this.addElement(c);
        }
    }
}
