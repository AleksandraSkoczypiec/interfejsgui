package app.model;

import javax.swing.table.DefaultTableModel;

public class MyTableModel extends DefaultTableModel{

    @Override
    public boolean isCellEditable(int row, int column) {

        return false;
    }

    public MyTableModel(int rowCount, int columnCount) {
        super(rowCount, columnCount);
    }

}
