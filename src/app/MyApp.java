package app;


import app.controller.WindowController;
import app.view.Window;
import javax.swing.*;

public class MyApp {

    private MyApp() {
        Window window = new Window();
        WindowController windowController = new WindowController(window);
        window.setWindowController(windowController);
        JFrame frame = window.createWindow();
        frame.setVisible(true);
        window.TipOfTheDay();
    }

    public static void main(String[] args) {
        new MyApp();
    }

}

//
//TODO
//w xmlu dokumentacja