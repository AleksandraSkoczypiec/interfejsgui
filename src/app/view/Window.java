package app.view;

import app.controller.WindowController;
import app.model.MyListModel;
import app.model.MyTableModel;
import com.l2fprod.common.swing.JTaskPane;
import com.l2fprod.common.swing.JTaskPaneGroup;
import com.l2fprod.common.swing.JTipOfTheDay;
import com.l2fprod.common.swing.tips.DefaultTip;
import com.l2fprod.common.swing.tips.DefaultTipModel;
import org.freixas.jcalendar.DateEvent;
import org.freixas.jcalendar.DateListener;
import org.freixas.jcalendar.JCalendarCombo;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Window {

    private WindowController windowController;

    private JTextArea textArea;
    private JTable table;
    private JTextField inputNumber;
    private JList list;
    private JPanel panelLeft = new JPanel();
    private JTextArea resultTextArea;

    private int MAX_ROW = 5;
    private int MAX_COL = 5;

    private int row = 1;
    private int col = 1;

    public JFrame createWindow() {
        JFrame frame = new JFrame("MyApp");

        frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        frame.pack();
        frame.setSize(875, 510);
        frame.setLocationRelativeTo(null);
        frame.setJMenuBar(creatingMenu());

        Container pane = frame.getContentPane();
        pane.add(creatingToolBar(), BorderLayout.PAGE_START);
        Container pane2 = new Container();

        GridBagConstraints c = new GridBagConstraints();
        pane2.setLayout(new GridBagLayout());


        c.fill = GridBagConstraints.HORIZONTAL;     //slider dla wiersza
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 0.5;
        c.insets = new Insets(5, 5, 5, 5);
        c.anchor = GridBagConstraints.LINE_START;
        pane2.add(createPanelWithSlider("Row:", JSlider.HORIZONTAL, MAX_ROW), c);

        c.fill = GridBagConstraints.HORIZONTAL;     //slider dla kolumny
        c.gridx = 1;
        c.gridy = 1;
        c.weightx = 0.5;
        c.insets = new Insets(5, 5, 5, 5);
        c.anchor = GridBagConstraints.LAST_LINE_START;
        pane2.add(createPanelWithSlider("Column:", JSlider.VERTICAL, MAX_COL), c);

        c.fill = GridBagConstraints.HORIZONTAL;     //tabela
        c.gridx = 0;
        c.gridy = 3;
        c.weightx = 1;
        c.gridwidth = 2;
        c.insets = new Insets(5, 5, 5, 5);
        pane2.add(creatingTable(), c);

        c.fill = GridBagConstraints.HORIZONTAL;     //wyswietlenie listy
        c.gridx = 0;
        c.gridy = 4;
        c.weightx = 0.5;
        c.gridwidth = 1;
        c.insets = new Insets(5, 5, 5, 5);
        pane2.add(creatingListsForMathOperations(), c);

        c.fill = GridBagConstraints.HORIZONTAL;     // textfield wprowadzenie liczby
        c.gridx = 0;
        c.gridy = 2;
        c.weightx = 0.5;
        c.gridwidth = 1;
        c.insets = new Insets(5, 5, 5, 5);
        pane2.add(creatingTextFieldForNumber(), c);

        c.fill = GridBagConstraints.HORIZONTAL;     //button dla wprowadzania liczby 'ok'
        c.gridx = 1;
        c.gridy = 2;
        c.weightx = 0.5;
        c.gridwidth = 1;
        c.insets = new Insets(5, 5, 5, 5);
        pane2.add(createButton(null, "OK", "OK", "OK"), c);

        c.fill = GridBagConstraints.HORIZONTAL;     //button dla wyniku 'result'
        c.gridx = 1;
        c.gridy = 5;
        c.weightx = 0.5;
        c.gridwidth = 1;
        c.insets = new Insets(5, 5, 5, 5);
        pane2.add(createButton(null, "RESULT", "RESULT", "Result"), c);

        c.fill = GridBagConstraints.HORIZONTAL;     //texfield dla wyniku
        c.gridx = 1;
        c.gridy = 4;
        c.weightx = 0.5;
        c.gridwidth = 1;
        c.insets = new Insets(5, 5, 5, 5);
        pane2.add(creatingTextAreaForResult(), c);

        c.fill = GridBagConstraints.HORIZONTAL; //JTaskPane
        c.gridx = 0;
        c.gridy = 5;
        c.weightx = 0.5;
        c.gridwidth = 1;
        c.insets = new Insets(5, 5, 5, 5);
        pane2.add(creatingCalendar(), c);


        Container pane3 = new Container();
        FlowLayout flowLayout = new FlowLayout();
        pane3.setLayout(flowLayout);
        pane3.add(createButton(null, "SAVE", "SAVE", "SAVE"));
        pane3.add(createButton(null, "EXIT", "EXIT", "EXIT"));
        pane3.add(createButton(null, "RESET", "RESET", "RESET"));
        pane3.add(createButton(null, "AVERAGE", "AV", "AV"));
        pane3.add(createButton(null, "MIN&MAX", "MINMAX", "MINMAX"));
        pane3.add(createButton(null, "RANDOM", "RANDOM", "RANDOM"));
        pane3.add(createButton(null, "INSTRUCTION", "INSTRUCTION", "INSTRUCTION"));
        pane3.add(createButton(null, "ABOUT", "ABOUT", "ABOUT"));

        Container pane4 = new Container();
        //FlowLayout flowLayout1 = new FlowLayout();
        pane4.setLayout(flowLayout);
        pane4.add(panelLeft());

        pane.add(pane4, BorderLayout.LINE_END);
        pane.add(pane2, BorderLayout.CENTER);
        pane.add(pane3, BorderLayout.PAGE_END);

        frame.addWindowListener(new WindowAdapter() {

            public void windowClosing(WindowEvent e) {
                if (JOptionPane.showConfirmDialog(null, "Are you sure?", "Warning",
                        JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                    System.exit(0);
                }
            }
        });

        return frame;
    }

    private JMenu createJMenu(String text, int shortCut) {
        JMenu jMenu = new JMenu(text);
        jMenu.setMnemonic(shortCut);
        return jMenu;
    }

    private JMenuItem createJMenuItem(String text, int shortCut, String actionCommand) {
        JMenuItem jmenuItem = new JMenuItem(text, shortCut);
        jmenuItem.setActionCommand(actionCommand);
        jmenuItem.addActionListener(windowController);
        return jmenuItem;
    }

    private JMenuBar creatingMenu() {

        JMenuBar menuBar = new JMenuBar();

        JMenu menu = createJMenu("File", KeyEvent.VK_F);
        menu.add(createJMenuItem("Save", KeyEvent.VK_R, "SAVE"));
        menu.add(createJMenuItem("EXIT", KeyEvent.VK_E, "EXIT"));
        menuBar.add(menu);

        menu = createJMenu("Options", KeyEvent.VK_F);
        menu.add(createJMenuItem("Reset", KeyEvent.VK_R, "RESET"));
        menu.add(createJMenuItem("Sum", KeyEvent.VK_S, "SUM"));
        menu.add(createJMenuItem("Average", KeyEvent.VK_V, "AV"));
        menu.add(createJMenuItem("MIN and MAX", KeyEvent.VK_M, "MINMAX"));
        menuBar.add(menu);

        menu = createJMenu("Help", KeyEvent.VK_E);
        menu.add(createJMenuItem("ABOUT", KeyEvent.VK_A, "ABOUT"));
        menu.add(createJMenuItem("INSTRUCTION", KeyEvent.VK_I, "INSTRUCTION"));
        menuBar.add(menu);

        return menuBar;
    }

    private JButton createButton(String imageFileName, String toolTipText, String actionCommand, String text) {
        JButton button = new JButton();
        if (text != null && !text.isEmpty()) {
            button.setText(text);
        }
        try {
            if (imageFileName != null) {
                Image img = ImageIO.read(getClass().getResource("../../" + imageFileName));
                button.setIcon(new ImageIcon(img));
            }
            button.addActionListener(windowController);
            button.setToolTipText(toolTipText);
            button.setActionCommand(actionCommand);
        } catch (IOException ex) {
            System.out.println(ex.toString());
        }
        return button;
    }

    private JToolBar creatingToolBar() {
        JToolBar toolBar = new JToolBar("Narzędzia");
        toolBar.setSize(50, 20);
        toolBar.setFloatable(false);

        toolBar.add(createButton("save.png", "Save", "SAVE", null));
        toolBar.add(createButton("reset.png", "Reset", "RESET", null));
        toolBar.add(createButton("sum.png", "Sum", "SUM", null));
        toolBar.add(createButton("average.png", "Average", "AV", null));
        toolBar.add(createButton("minmax.png", "MINMAX", "MINMAX", null));
        toolBar.add(createButton("exit.png", "Exit", "EXIT", null));
        toolBar.add(createButton("random.png", "Random", "RANDOM", null));
        toolBar.add(createButton("instruction.png", "Instruction", "INSTRUCTION", null));
        toolBar.add(createButton("bear.png", "About...", "ABOUT", null));

        return toolBar;
    }

    private JPanel createPanelWithSlider(String text, int orientation, int max) {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        JLabel sliderLabel = new JLabel(text);
        JSlider jSlider = new JSlider(JSlider.HORIZONTAL, 1, max, 1);
        if (orientation == 0) {
            jSlider.addChangeListener(e -> row = jSlider.getValue());
        } else if (orientation == 1) {
            jSlider.addChangeListener(e -> col = jSlider.getValue());
        }
        sliderLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        jSlider.setAlignmentX(Component.CENTER_ALIGNMENT);
        jSlider.setMinorTickSpacing(1);
        jSlider.setMajorTickSpacing(1);
        jSlider.setPaintTicks(true);
        jSlider.setPaintLabels(true);
        panel.add(sliderLabel);
        panel.add(jSlider);

        return panel;
    }

    private JTextField creatingTextFieldForNumber() {
        inputNumber = new JTextField(3);
        inputNumber.setBorder(BorderFactory.createTitledBorder("Insert number here and confirm with OK button "));
        return inputNumber;
    }

    private JTable creatingTable() {

        MyTableModel myTableModel = new MyTableModel(MAX_ROW, MAX_COL);
        table = new JTable(myTableModel);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        for (int row = 0; row < table.getRowCount(); row++) {
            for (int col = 0; col < table.getColumnCount(); col++) {
                table.setValueAt(0.0, row, col);
                rightRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
                table.getColumnModel().getColumn(col).setCellRenderer(rightRenderer);
            }
        }
        return table;
    }

    private JList creatingListsForMathOperations() {
        MyListModel<String> listModel = new MyListModel<>();
        listModel.addElements("Sum", "Average", "MIN and MAX");
        list = new JList<>(listModel);
        list.setBorder(BorderFactory.createTitledBorder("Available math operations"));
        return list;
    }

    private JTextArea creatingTextAreaForResult() {

        textArea = new JTextArea(4, 2);
        textArea.setEditable(false);
        textArea.setBorder(BorderFactory.createTitledBorder("Result of operation"));
        return textArea;
    }

    public JTextArea getTextArea() {
        return textArea;
    }

    public JTable getTable() {
        return table;
    }

    public JTextField getInputNumber() {
        return inputNumber;
    }

    public JList getList() {
        return list;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public void setWindowController(WindowController windowController) {
        this.windowController = windowController;
    }

    public JTaskPane panelLeft() {
        JTaskPane taskPane = null;

        taskPane = new JTaskPane();
        JTaskPaneGroup groupFile = new JTaskPaneGroup();
        JTaskPaneGroup groupCalc = new JTaskPaneGroup();
        JTaskPaneGroup groupHelp = new JTaskPaneGroup();

        groupFile.setTitle("File");
        groupCalc.setTitle("Operations");
        groupHelp.setTitle("Help");

        taskPane.add(groupFile);
        taskPane.add(groupCalc);
        taskPane.add(groupHelp);

        groupFile.add(createButton("save.png", "Save", "SAVE", null));
        groupFile.add(createButton("exit.png", "Exit", "EXIT", null));
        groupCalc.add(createButton("reset.png", "Reset", "RESET", null));
        groupCalc.add(createButton("sum.png", "Sum", "SUM", null));
        groupCalc.add(createButton("average.png", "Average", "AV", null));
        groupCalc.add(createButton("minmax.png", "MINMAX", "MINMAX", null));
        groupCalc.add(createButton("random.png", "Random", "RANDOM", null));
        groupHelp.add(createButton("instruction.png", "Instruction", "INSTRUCTION", null));
        groupHelp.add(createButton("bear.png", "About", "ABOUT", null));


        taskPane.setBackground(new Color(238, 238, 238));
        return taskPane;
    }


    public JCalendarCombo creatingCalendar() {

        JCalendarCombo calendar = new JCalendarCombo();

        DateListener listen = new DateListener() {

            @Override
            public void dateChanged(DateEvent dateEvent) {
                textArea.setText("");
                Calendar c = dateEvent.getSelectedDate();
                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
                String formatted = format1.format(c.getTime());
                textArea.append("Chosen data: " + formatted + "\n");

            }
        };

        calendar.setBounds(0, 240, 575, 60);
        calendar.addDateListener(listen);
        calendar.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));


        return calendar;
    }

    public void TipOfTheDay(){

        DefaultTipModel defaultTipModel = new DefaultTipModel();
        defaultTipModel.add(new DefaultTip("Did you know...","Model–view–controller (MVC) is a software architectural pattern for implementing user interfaces on computers. It divides a given application into three interconnected parts in order to separate internal representations of information from the ways that information is presented to and accepted from the user. The MVC design pattern decouples these major components allowing for efficient code reuse and parallel development.."));
        defaultTipModel.add(new DefaultTip("Did you know...","The Tetris effect (also known as Tetris Syndrome) occurs when people devote so much time and attention to an activity that it begins to pattern their thoughts, mental images, and dreams. It takes its name from the video game Tetris."));
        JTipOfTheDay tipOfTheDay = new JTipOfTheDay(defaultTipModel);
        tipOfTheDay.showDialog(tipOfTheDay);

    }



}